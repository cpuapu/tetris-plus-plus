
#ifndef _GAME_H_
#define _GAME_H_

#include "gameParameters.h"

#include "scoresList.h"

#include "mainMenu.h"

enum GAME_SPEED
{
	EASY = 800,
	MEDUIM = 400,
	HARD = 200
};

class Game
{

	GameParameters* params;
	ScoresList* gameScores;
	int gameSpeed; // speed in mSec
	
	MainMenu menu;

	void updateGameSpeed(GameParameters* prms);
	bool getInput(char& c);

	void startSinglePlayer();
	void startMultiPlayer();

	void printGameControls();


public:
	Game();
	~Game();
	void run();
	
};



#endif