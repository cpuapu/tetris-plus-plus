
#ifndef _SCORESLIST_H_
#define _SCORESLIST_H_



struct Player
{
	int score;
	char name[20];
	
};

class ScoresList
{
	Player scores[10];
	int maxScore;
	int minScore;
	int size; // logical size
	Player tempPlayer;

	void insertPlayer(Player& p);
	
	void getPlayerName();

	bool isTopTen(int score);


	void sort();

public:

	ScoresList();
	
	void addScore(int score);

	void printScoreList();

	

};

int comparePlayers(const void * a, const void * b);


#endif