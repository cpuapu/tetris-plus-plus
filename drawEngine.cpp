
#include "drawEngine.h"
#include <Windows.h>
#include <iostream>
#include <fstream>

using namespace std;

DrawEngine::DrawEngine(GameParameters* p)
{
	params = p;
	isCurserVisible(false);
}

DrawEngine::~DrawEngine()
{
	isCurserVisible(true);
}

void DrawEngine::gotoxy(int x, int y)
{
	HANDLE output_handle;
	COORD pos;
	pos.X = x;
	pos.Y  = y;
	output_handle = GetStdHandle(STD_OUTPUT_HANDLE);

	SetConsoleCursorPosition(output_handle, pos);
}

void DrawEngine::gotoxy(Point* p)
{
	gotoxy(p->x,p->y);
}

void DrawEngine::isCurserVisible(bool visible)
{
	HANDLE output_handle;

	CONSOLE_CURSOR_INFO cciInfo;
	cciInfo.bVisible = visible;
	cciInfo.dwSize = sizeof(CONSOLE_CURSOR_INFO);

	output_handle = GetStdHandle(STD_OUTPUT_HANDLE);

	SetConsoleCursorInfo(output_handle,&cciInfo);
}

void DrawEngine::deleteShape(Point *p)
{
	for(int i= 0 ; i < 4 ; ++i)
	{
		shapePoints[i].x = p[i].x;
		shapePoints[i].y = p[i].y;

	}
}



void DrawEngine::drawShape(Point* p)
{
	char c=  params->getShapeStyle();
	gotoxy(p[0].x,p[0].y);
	cout << c;
	gotoxy(p[1].x,p[1].y);
	cout << c;
	gotoxy(p[2].x,p[2].y);
	cout << c;
	gotoxy(p[3].x,p[3].y);
	cout << c;
}

void DrawEngine::eraseShape(Point* p)
{
	
	gotoxy(p[0].x,p[0].y);
	cout << ' ';
	gotoxy(p[1].x,p[1].y);
	cout << ' ';
	gotoxy(p[2].x,p[2].y);
	cout << ' ';
	gotoxy(p[3].x,p[3].y);
	cout << ' ';

}


void DrawEngine::drawMap(char** map)
{
	
	char c = (char)178; //wall
	Point startP = params->getGameStartingPoint();
#ifdef DEBUG_A
	ofstream out;
	out.open("test.txt");
#endif
		for ( int i = 0 ; i < params->getScreenHeight() ; ++i)
		{
	
			for ( int j =0 ; j <  params->getScreenWidth()  ; ++j) 
			{
				gotoxy((startP.x)+j,(startP.y)+i);

				if ( map[ j][ i] == 1 ) //wall
				{
					cout << c ;
					#ifdef DEBUG_A
					out << '1';
					#endif
				}
				else if ( map[ j][ i] == 2 )
				{
					cout << params->getShapeStyle();
					#ifdef DEBUG_A
					out << '2';
					#endif
				}
				else if ( map[ j][ i] == 0 )
				{
					cout << ' ';
					#ifdef DEBUG_A
					out << '0';
					#endif
				}
			}
			#ifdef DEBUG_A
			out << '\n';
			#endif
		
		}
		#ifdef DEBUG_A
		out.close();
		#endif

}

void DrawEngine::drawScore(int score)
{
	Point startP = params->getGameStartingPoint();

	gotoxy(startP.x + 2 , startP.y - 2);

	cout << "score: " << score;
}