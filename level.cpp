

#include "Level.h"

#include <cstdlib>
#include <Windows.h>
#include <conio.h>
#include <iostream>

using namespace std;

Level::Level(GameParameters* p, DrawEngine* d) : map(0), params(0), de(0), fullLines(0) , character(0), shp(0)
{

	params = p;
	de = d;
	

	map = new char*[params->getScreenWidth()];

	for ( int i = 0 ; i  < params->getScreenWidth() ; ++i)
		map[i] = new char[params->getScreenHeight()];


	
	clearMap();
	setLevelMap();
	drawLevelMap();

	shp = new Shape(de);
	character = new GameCharacter(shp,params);


	shp->setMap(map);
	gameStartP = new Point;
	*gameStartP = params->getGameStartingPoint();
	shp->setGameStartingPoint(gameStartP);
	
	params->setScore(0);


}


Level::~Level()
{

	for ( int i = 0 ; i  < params->getScreenWidth() ; ++i)
		delete [] map[i];

	
	delete [] map;

	delete [] fullLines;
	delete character;
	delete shp;
	delete gameStartP;
	
	
}

void Level::clearMap()
{
	for ( int i = 0 ; i < params->getScreenWidth() ; ++i)
	{
		for ( int j = 0 ; j  < params->getScreenHeight() ; ++j)
		{
			map[i][j] = SPACE;
		}
	}

}

void Level::setLevelMap()
{
	Point startP = params->getGameStartingPoint();
	int gameDirection = params->getGameDirection();

	if ( gameDirection == UTD)
	{
		//bottom wall
		for ( int i = 0 ; i < params->getScreenWidth() ; ++i)
		{
			map[i][ params->getScreenHeight()-1] = WALL;
			
		}

		//left wall
		for ( int i = 0 ; i < params->getScreenHeight() ; ++i)
		{
			map[ 0 ][i ] = WALL;
			
		}

		// right wall
		for ( int i = 0 ; i < params->getScreenHeight() ; ++i)
		{
			map[ params->getScreenWidth()-1 ][ i ]= WALL;
			
		}
	}
 	else if ( gameDirection == RTL)
	{
		//up wall
		for ( int i = 0 ; i < params->getScreenWidth() ; ++i)
		{
			map[i][0] = WALL;
			
		}

		//left wall
		for ( int i = 0 ; i < params->getScreenHeight() ; ++i)
		{
			map[0][i] = WALL;
		
		}

		// bottom wall
		for ( int i = 0 ; i < params->getScreenWidth() ; ++i)
		{
			map [i][params->getScreenHeight()-1] = WALL;
			
		}
	}
	else if ( gameDirection == LTR)
	{
		//up wall
		for ( int i = 0 ; i < params->getScreenWidth() ; ++i)
		{
			map[i][0] = WALL;
			
		}

		//right wall
		for ( int i = 0 ; i < params->getScreenHeight() ; ++i)
		{
			map[params->getScreenWidth()-1][i] = WALL;
		
		}

		// bottom wall
		for ( int i = 0 ; i < params->getScreenWidth() ; ++i)
		{
			map [i][params->getScreenHeight()-1] = WALL;
			
		}
	}

	else if (gameDirection == DTU)
	{
		// up wall
		for ( int i = 0 ; i < params->getScreenWidth() ; ++i)
		{
			map[i][0] = WALL;
			
		}

		//left wall
		for ( int i = 0 ; i < params->getScreenHeight() ; ++i)
		{
			map[ 0 ][i ] = WALL;
			
		}

		// right wall
		for ( int i = 0 ; i < params->getScreenHeight() ; ++i)
		{
			map[ params->getScreenWidth()-1 ][ i ]= WALL;
			
		}
	}


	
}

void Level::drawLevelMap()
{
	de->drawMap(map);
}


char** Level::getMap()
{
	return map;
}


bool Level::isFullLine(int index)
{
	int fullLine;
	int calculatedIndex;
	

	if ( params->getGameDirection()  == UTD)
	{
		fullLine = params->getScreenWidth() - 2;
		calculatedIndex = params->getScreenHeight()-2 -index;
		for ( int i = 1 ; i <= (params->getScreenWidth() - 2) ; ++i)
		{
			if (map[i][calculatedIndex] == SHAPE)
				fullLine--;
		}

	}
	else if ( params->getGameDirection()   == RTL)
	{
		fullLine = params->getScreenHeight() - 2;
		calculatedIndex =  1 + index;
		
		for ( int i = 1 ; i <= (params->getScreenHeight() - 2) ; ++i)
		{
			if (map[calculatedIndex][i] == SHAPE)
				fullLine--;
		}
	}
	else if( params->getGameDirection()   == LTR)
	{
		fullLine = params->getScreenHeight() - 2;
		calculatedIndex = params->getScreenWidth() - 2 - index;
		for ( int i = 1 ; i <= (params->getScreenHeight() - 2) ; ++i)
		{
			if (map[calculatedIndex][i] == SHAPE)
				fullLine--;
		}

	}
	else if( params->getGameDirection()   == DTU)
	{
		fullLine = params->getScreenWidth() - 2;
		calculatedIndex = index+1;
		for ( int i = 1 ; i <= (params->getScreenWidth() - 2) ; ++i)
		{
			if (map[i][calculatedIndex] == SHAPE)
				fullLine--;
		}
	}

	return (fullLine == 0);
}

bool Level::hasFullLine()
{
	int numLines;
	

	if( ( params->getGameDirection() == UTD ) || ( params->getGameDirection() == DTU )  )
		numLines = params->getScreenHeight() -1;
	else
		numLines = params->getScreenWidth() -1;

	fullLines = new bool[numLines]; // delete in d'tor
		
	bool fullLine = true;
	

	for ( int i = 0 ;  (i < numLines) && fullLine ; i++)
	{
		fullLines[i] = isFullLine(i);
		
		if ( fullLines[i] == false)
			fullLine = false;
			
		// solution: using a bool array.. this will work/solve if the line not the first line.. 
	}
	
	


	// assuming that removing lines starts on the bottom of the game
	// if we have a line in any index, which between two "not full lines" will not be removed
	
	int numFullLines; // count the full lines; 
	for ( numFullLines = 0 ; fullLines[numFullLines] ; ++numFullLines);



	if ( fullLines[0])
	{
		updateScore(numFullLines);
		updateMap(numFullLines);
		drawLevelMap();
	}
	return fullLines[0];
}

void Level::moveLine(int src, int des)
{
	
	
	
	int realSrc, realDes;

	if( params->getGameDirection() == UTD)
	{
		realSrc = params->getScreenHeight()-2 -src;
		realDes = params->getScreenHeight()-2 -des;

		for ( int i = 1 ; i <= (params->getScreenWidth() -2) ; i++)
		{
			if( realSrc > (params->getScreenHeight()-1) )
				map[i][realDes] = SPACE;
			else if (realSrc <= 0 || realDes <= 0)
				map[i][realDes] = SPACE;
			else
			{
				map[i][realDes] = map[i][realSrc];
				map[i][realSrc] = SPACE;
			}
		}
	}
	else if (params->getGameDirection() == RTL)
	{
		realSrc = 1 + src;
		realDes = 1 + des;
		
		for ( int i = 1; i <= (params->getScreenHeight() - 2) ; ++i)
		{
			
			if ( realSrc >= (params->getScreenWidth()-1)) 
				map[realDes][i] = SPACE ; 
				
			else if (realSrc <= 0 || realDes <= 0)
				map[realDes][i] = SPACE;
			else
			{
				map[realDes][i] = map[realSrc][i];
				map[realSrc][i] = SPACE;
			}
		}
	}
	else if(params->getGameDirection() == LTR)
	{
		
		realSrc = params->getScreenWidth() - 2 - src;
		realDes = params->getScreenWidth() - 2 - des;

	
		for ( int i = 1; i <= (params->getScreenHeight() - 2) ; ++i)
		{
			
			if ( realSrc >= (params->getScreenWidth() -1) )
				map[realDes][i] = SPACE;
			else  if (realSrc <= 0 || realDes <= 0)
				
				break;
			else
			{
				map[realDes][i] = map[realSrc][i];
				map[realSrc][i] = SPACE;
			}
		}
	}

	else if( params->getGameDirection() == DTU)
	{
		realSrc = src + 1;
		realDes = des + 1;

		for ( int i = 1 ; i <= (params->getScreenWidth() -2) ; i++)
		{
			if( realSrc >= (params->getScreenHeight()-1) )
				break;
			else if (realSrc <= 0 || realDes <= 0)
				map[i][realDes] = SPACE;
			else
			{
				map[i][realDes] = map[i][realSrc];
				map[i][realSrc] = SPACE;
			}
		}
	}

	

}

void Level::updateMap(int numLines)
{
	int lines = numLines;
	

	if( (params->getGameDirection() == UTD)||  (params->getGameDirection() == DTU))
	{
		
		for( int i = 0 ; i <= ( params->getScreenHeight() - 1 - lines) ; ++i)
		{
			moveLine(numLines++,i);
			
		}
		
	}
	else if ( (params->getGameDirection() == RTL ) || (params->getGameDirection() == LTR) )
	{
		for ( int i = 0 ; i <= (params->getScreenWidth() -1 - lines) ; ++i)
		{
			moveLine(numLines++,i);
		}
	}
}


bool Level::isEndOfGame()
{
	bool isEnd = false;
	if( params->getGameDirection() == UTD)
	{
		for ( int i = 1 ; (i <= (params->getScreenWidth() - 2) ) && !isEnd ; ++i)
		{
			if ( map[i][2]  == SHAPE)
				isEnd = true;
		}
	}
	else if ( params->getGameDirection() == RTL)
	{
		for ( int i = 1 ; (i <= (params->getScreenHeight() - 2) ) && !isEnd ; ++i)
		{
			if ( map[params->getScreenWidth() -1 -2 ][i]  == 2)
				isEnd = true;
		}
	}
	else if (params->getGameDirection() == LTR )
	{
		for ( int i = 1 ; (i <= (params->getScreenHeight() - 2) ) && !isEnd ; ++i)
		{
			if ( map[2][i]  == SHAPE)
				isEnd = true;
		}
	}
	else if( params->getGameDirection() == DTU)
	{
		for ( int i = 1 ; (i <= (params->getScreenWidth() - 2) ) && !isEnd ; ++i)
		{
			if ( map[i][params->getScreenHeight() -1 -3 ]  == SHAPE)
				isEnd = true;
		}
	}

	return isEnd;

}


void Level::keyPress(char& c)
{
	
	character->KeyPress(&c);
	
}

void Level::start()
{
	/*
	Point moveDirec = params->getMoveDirection();
	Point newShpPos = params->getNewShapePostion();
	
	char key = ' ';
	shp->generateShape(newShpPos, rand() % 8); // mod shapes num + 1

	// draw score
	de->drawScore(0);

	
	while( !isEndOfGame() && key != 27)
	{
	
		while(!getInput(key) && shp->isValidMove(moveDirec.x,moveDirec.y))
		{*/
		shp->move(params->getMoveDirection());
	/*		Sleep(gameSpeed);
			
		}
		keyPress(key);
		if ( key != 27)
			key = ' ';
		
		
		if(!shp->isValidMove(moveDirec.x,moveDirec.y) && !isEndOfGame())
		{
			shp->saveToMap();
			hasFullLine();
			shp->generateShape(newShpPos, rand() % 8);
			
		}
	}

	// put the score to score list

	getchar(); // and puts game over
	*/
}

bool Level::getInput(char& c)
{
	if(_kbhit())
	{
		c = _getch();
		return true;
	}

	return false;
}



void Level::updateScore(int numOfLines)
{
	switch(numOfLines)
	{

	case 1:
		params->increaseScoreBy(100);
		break;

	case 2:
		params->increaseScoreBy(200);
		break;

	case 3:
		params->increaseScoreBy(400);
		break;

	case 4:
		params->increaseScoreBy(800);
		break;

	default:
		params->increaseScoreBy(800);
		break;
	}

	de->drawScore(params->getScore());
}



void Level::initLevel()
{



	shp->generateShape(params->getNewShapePostion(), rand() % 8); // mod shapes num + 1

	// draw score
	de->drawScore(0);
}

void Level::update()
{
	shp->saveToMap();
	hasFullLine();
	shp->generateShape(params->getNewShapePostion(), rand() % 8); // mod shapes num + 1
}

bool Level::isValidMove()
{
	return shp->isValidMove(params->getMoveDirection());
}
