


#ifndef _LEVEL_H_
#define _LEVEL_H_


#include "drawEngine.h"
#include "character.h"


enum board
{
	SPACE,
	WALL,
	SHAPE
};



class Level
{

	char** map;
	GameParameters* params;
	DrawEngine* de;
	bool* fullLines;
	GameCharacter* character;
	Shape* shp;
	Point* gameStartP;


	void setLevelMap();
	void drawLevelMap();

	bool isFullLine(int index);

	void moveLine(int src, int des);
	void updateMap(int numLines);

	void clearMap();
	

	void updateScore(int numOfLines);

public:

	Level(GameParameters* p, DrawEngine* d);
	~Level();

	void keyPress(char& c);

	bool hasFullLine();

	char** getMap();

	void start();

	bool getInput(char& c);

	bool isEndOfGame();
	
	bool isValidMove();

	void initLevel(); // generate shape,, draw score 0
	
	void update();

	

};


#endif