
#ifndef _CHARA_H_
#define _CHARA_H_

#include "shape.h"
#include "gameParameters.h"


class GameCharacter
{

	Shape* shape;
	GameParameters* params;


	void rightKeyPress();
	void leftKeyPress();
	void downKeyPress();
	void rotateKeyPress();
	void pauseKeyPress();
	void undoKeyPress();
	void upKeyPress();


public:
	
	GameCharacter(Shape* inShape, GameParameters* par); 
	~GameCharacter();

	void KeyPress(char* c);
	Shape* getShape();
	void setShape(Shape* inShape);

};

#endif