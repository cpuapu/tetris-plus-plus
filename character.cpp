
#include "character.h"
#include <Windows.h>

GameCharacter::GameCharacter(Shape* inShape, GameParameters* par) :shape(0) , params(0)
{
	shape = inShape;
	params= par;
}


GameCharacter::~GameCharacter()
{

}

Shape* GameCharacter::getShape()
{
	return shape;
}

void GameCharacter::setShape(Shape* inShape)
{
	shape = inShape;
	
}

void GameCharacter::KeyPress(char* c)
{

	if ( *c == params->getRightkey() )
	{
		rightKeyPress();
	}
	else if ( *c ==  params->getLeftKey())
	{
		leftKeyPress();
	}
	else if( *c == params->getDownKey())
	{
		downKeyPress();
	}
	else if( *c == params->getUpKey() )
	{
		upKeyPress();
	}
	else if( *c == params->getPauseKey() )
	{
		pauseKeyPress();
	}
	else if( *c == params->getRotationKey() )
	{
		rotateKeyPress();
	} 
	else if( *c == params->getUndoKey() )
	{
		undoKeyPress();
	}
	else
	{
	}
	
}


void GameCharacter::rightKeyPress()
{
	shape->move(1,0);
}

void GameCharacter::leftKeyPress()
{
	shape->move(-1,0);
}

void GameCharacter::downKeyPress()
{
	shape->move(0,1);
}

void GameCharacter::upKeyPress()
{
	shape->move(0,-1);
}

void GameCharacter::rotateKeyPress()
{
	shape->rotate();
	
}

void GameCharacter::pauseKeyPress()
{
	Sleep(1000);
}

void GameCharacter::undoKeyPress()
{
	
	shape->eraseLastShape();
}


