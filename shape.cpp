
#include "shape.h"


Shape::Shape(DrawEngine* de) : pde(0) , map(0) 
{
	pde = de;
	for ( int i = 0; i < 4 ; ++i)
	{
		shapePoints[i].x =lastShape[i].x =0;
		shapePoints[i].y =lastShape[i].y=  0;
		
		
	}

	isLastShapeRemoved = true;
	
}

Shape::Shape(DrawEngine* de, Point* inputShape) : pde(0) , map(0) 
{
	pde = de;

	for ( int i = 0; i < 4 ; ++i)
	{
		shapePoints[i].x =lastShape[i].x = 0;
		shapePoints[i].y =lastShape[i].y=  0;
		
	}


	isLastShapeRemoved = true;
	
}


Shape::Shape(DrawEngine* de, Point* inputShape, int inShapeID) :pde(0), map(0)  
{
	pde = de;

	//generateShape(inShapeID);
	
	for ( int i = 0; i < 4 ; ++i)
	{
		shapePoints[i].x =lastShape[i].x =0;
		shapePoints[i].y =lastShape[i].y=  0;
		
	}
	isLastShapeRemoved = true;
	
}
Shape::~Shape()
{
}




bool Shape::move(int x, int y)
{
	if (isValidMove(x,y))
	{
		erase(shapePoints);
	
		for(int i = 0 ; i < 4 ; ++i)
		{
			shapePoints[i].x += x;
			shapePoints[i].y += y;
		}

		draw(shapePoints);
	
		return true; // not done yet
	}
	
	return false;
}


bool Shape::move(Point& p)
{
	return move(p.x,p.y);
}

bool Shape::isValidMove(int x, int y)
{
	Point tempPoints[4];
	int isVaildFourPoint = 0 ; // if the number is 4


	for(int i = 0 ; i < 4 ; ++i)
	{
		tempPoints[i].x = shapePoints[i].x - gameStartingPoint.x + x;
		tempPoints[i].y = shapePoints[i].y - gameStartingPoint.y + y;

		if( map[tempPoints[i].x][tempPoints[i].y] == 0 )
			isVaildFourPoint++;
	}

	
	return  (isVaildFourPoint == 4);
}

bool Shape::isValidMove(Point& p)
{
	return isValidMove(p.x, p.y);
}

Point* Shape::getShape()
{
	return shapePoints;
}

void Shape::setShape(Point* p)
{
	// 4 = shape size
	for(int i = 0 ; i < 4 ; ++i)
	{
		shapePoints[i].x = p[i].x;
		shapePoints[i].y = p[i].y;
	}
}


void Shape::draw(Point* p)
{
	pde->drawShape(p);
	
}

void Shape::erase(Point* p)
{
	pde->eraseShape(p);
	
}


bool Shape::rotate()
{
	if(!isValidRotation())
		return false;

	if( shapeID == SQUARE)
	{
		// no rotation!
	}
	else if ( shapeID == LINE)
	{
		if (( shapeAngle == RIGHT) || (shapeAngle == LEFT) )
		{
			erase(shapePoints);
			shapePoints[0].x = shapePoints[1].x;
			shapePoints[0].y = shapePoints[0].y-1; 

			shapePoints[2].x = shapePoints[1].x;
			shapePoints[2].y = shapePoints[1].y+1;
	
			shapePoints[3].x = shapePoints[1].x;
			shapePoints[3].y = shapePoints[1].y+2;
			draw(shapePoints);

			shapeAngle=1;
		}
		else 
		{
			erase(shapePoints);
			shapePoints[0].x -= 1; 
			

			
			shapePoints[1].y -= 1; 

			shapePoints[2].x += 1; 
			shapePoints[2].y -= 2;
	
			shapePoints[3].x += 2 ;
			shapePoints[3].y -= 3;
			draw(shapePoints);
			shapeAngle=0;
		}
	}
	else if( shapeID == TRIANGLE)
	{
		if ( shapeAngle == RIGHT)
		{
			erase(shapePoints);

			shapePoints[0].x -= 1;
			shapePoints[0].y += 1;

			draw(shapePoints);
			
			shapeAngle++;
			shapeAngle %= 4;
		}

		else if ( shapeAngle == BOTTOM )
		{
			erase(shapePoints);

			shapePoints[3].x -= 1;
			shapePoints[3].y -= 1;

			draw(shapePoints);

			shapeAngle++;
			shapeAngle %= 4;
		}

		else if ( shapeAngle == LEFT )
		{
			erase(shapePoints);
	

			shapePoints[0].x += 1;
			shapePoints[0].y -= 1;

			shapePoints[1].x -= 1;
			shapePoints[1].y -= 1;


			shapePoints[3].x += 1;
			shapePoints[3].y += 1;

			draw(shapePoints);

			shapeAngle++;
			shapeAngle %= 4;
		}
	
		else if ( shapeAngle == UP)
		{
			erase(shapePoints);
			shapePoints[1].x += 1;
			shapePoints[1].y += 1;
			draw(shapePoints);

			shapeAngle++;
			shapeAngle %= 4;
		}
	}
		
	else if( shapeID == Z)
	{
		if ( shapeAngle == RIGHT)
		{
			erase(shapePoints);
			
			shapePoints[0].x += 2;
			shapePoints[0].y -= 1;

			//shapePoints[1].x = 0;
			//shapePoints[1].y += 1;

			//shapePoints[2].x -= 1;
			//shapePoints[2].y = 0;

			//shapePoints[3].x -= 2;
			shapePoints[3].y -= 1;


			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
		else if ( shapeAngle == BOTTOM)
		{
			erase(shapePoints);
				
			shapePoints[0].x -= 2;
			shapePoints[0].y += 1;

			//shapePoints[1].x -= 1;
			//shapePoints[1].y += 1;

			//shapePoints[2].x = 0;
			//shapePoints[2].y = 0;

			//shapePoints[3].x -= 1;
			shapePoints[3].y += 1;


			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
		else if ( shapeAngle == LEFT)
		{
			erase(shapePoints);
				
			shapePoints[0].x += 2;
			shapePoints[0].y -= 1;

			//shapePoints[1].x = 0;
			//shapePoints[1].y += 1;

			//shapePoints[2].x -= 1;
			//shapePoints[2].y = 0;

			//shapePoints[3].x -= 2;
			shapePoints[3].y -= 1;



			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
		else if ( shapeAngle == UP)
		{
			erase(shapePoints);
			
			shapePoints[0].x -= 2;
			shapePoints[0].y += 1;

			//shapePoints[1].x -= 1;
			//shapePoints[1].y += 1;

			//shapePoints[2].x = 0;
			//shapePoints[2].y = 0;

			//shapePoints[3].x -= 1;
			shapePoints[3].y += 1;


			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
	}
	else if( shapeID == REVZ)
	{
		if ( shapeAngle == RIGHT)
		{
			erase(shapePoints);
			
			shapePoints[0].x -= 2;
			shapePoints[0].y -= 1;

			//shapePoints[1].x = 0;
			//shapePoints[1].y += 1;

			//shapePoints[2].x += 1;
			//shapePoints[2].y = 0;

			//shapePoints[3].x += 1;
			shapePoints[3].y -= 1;


			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
		else if ( shapeAngle == BOTTOM)
		{
			erase(shapePoints);
				
			shapePoints[0].x += 2;
			shapePoints[0].y += 1;

			//shapePoints[1].x -= 1;
			//shapePoints[1].y += 1;

			//shapePoints[2].x -= 1;
			//shapePoints[2].y = 0;

			//shapePoints[3].x -= 1;
			shapePoints[3].y += 1;


			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
		else if ( shapeAngle == LEFT)
		{
			erase(shapePoints);
			shapePoints[0].x -= 2;
			shapePoints[0].y -= 1;

			//shapePoints[1].x = 0;
			//shapePoints[1].y += 1;

			//shapePoints[2].x += 1;
			//shapePoints[2].y = 0;

			//shapePoints[3].x += 1;
			shapePoints[3].y -= 1;


			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
		else if ( shapeAngle == UP)
		{
			erase(shapePoints);
			
				
			shapePoints[0].x += 2;
			shapePoints[0].y += 1;

			//shapePoints[1].x -= 1;
			//shapePoints[1].y += 1;

			//shapePoints[2].x -= 1;
			//shapePoints[2].y = 0;

			//shapePoints[3].x -= 1;
			shapePoints[3].y += 1;

			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
		
	}
	else if( shapeID == L)
	{
		if ( shapeAngle == RIGHT)
		{
			erase(shapePoints);
				
			//shapePoints[0].x -= 1;
			shapePoints[0].y += 2;

			shapePoints[1].x -= 1;
			shapePoints[1].y += 1;

			//shapePoints[2].x = 0;
			//shapePoints[2].y = 0;

			shapePoints[3].x += 1;
			shapePoints[3].y -= 1;


			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
		else if ( shapeAngle == BOTTOM)
		{
			erase(shapePoints);
			
			shapePoints[0].x -= 1;
			//shapePoints[0].y -= 0;

			//shapePoints[1].x -= 0;
			shapePoints[1].y -= 1;

			shapePoints[2].x += 1;
			//shapePoints[2].y += 1;

			shapePoints[3].x += 2;
			shapePoints[3].y += 1;


			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
		else if ( shapeAngle == LEFT)
		{
			erase(shapePoints);
			
			//shapePoints[0].x = 0;
			shapePoints[0].y -= 1;

			shapePoints[1].x += 1;
			//shapePoints[1].y = 0; 

			//shapePoints[2].x -= 1;
			shapePoints[2].y += 1;

			shapePoints[3].x -= 1;
			shapePoints[3].y += 2;


			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
		else if ( shapeAngle == UP)
		{
			erase(shapePoints);
			
			shapePoints[0].x += 1;
			shapePoints[0].y -= 1;

			//shapePoints[1].x = 0;
			//shapePoints[1].y += 1;

			shapePoints[2].x -= 1;
			shapePoints[2].y -= 1;

			shapePoints[3].x -= 2;
			shapePoints[3].y -= 2;


			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
		
	}
	else if( shapeID == REVL)
	{
			
		if ( shapeAngle == RIGHT)
		{
			erase(shapePoints);
				
			//shapePoints[0].x = 0;
			//shapePoints[0].y = 0;

			shapePoints[1].x += 1;
			shapePoints[1].y += 1;

			shapePoints[2].x -= 1;
			//shapePoints[2].y = 0;

			shapePoints[3].x -= 2;
			shapePoints[3].y += 1;


			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
		else if ( shapeAngle == BOTTOM)
		{
			erase(shapePoints);
				
			//shapePoints[0].x = 0;
			//shapePoints[0].y = 0;

			//shapePoints[1].x += 1;
			shapePoints[1].y -= 2;

			shapePoints[2].x -= 1;
			shapePoints[2].y -= 1;

			shapePoints[3].x += 1;
			shapePoints[3].y -= 1;


			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
		else if ( shapeAngle == LEFT)
		{
			erase(shapePoints);
				
			//shapePoints[0].x -= 1;
			//shapePoints[0].y = 0;

			//shapePoints[1].x += 1;
			//shapePoints[1].y += 1;

			shapePoints[2].x += 2;
			shapePoints[2].y -= 1;

			//shapePoints[3].x += 2;
			shapePoints[3].y -= 3;


			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
		else if ( shapeAngle == UP)
		{
			erase(shapePoints);
				
			//shapePoints[0].x = 0;
			shapePoints[0].y -= 2;

			shapePoints[1].x -= 1;
			shapePoints[1].y -= 1;
	
			//shapePoints[2].x += 2;
			//shapePoints[2].y -= 1;

			shapePoints[3].x += 1;
			shapePoints[3].y += 1;


			draw(shapePoints);
			shapeAngle++;
			shapeAngle %= 4;
		}
		
		
	}

	return true;  // valid
}
bool Shape::isValidRotation()
{
	Point tempPoint[4];
	int isVaildFourPoint = 0 ; // if the number is 4

	for ( int i = 0 ; i < 4 ; ++i)
	{
		tempPoint[i].x = shapePoints[i].x;
		tempPoint[i].y = shapePoints[i].y;
	}

	if( shapeID == SQUARE)
	{
		// no rotation!
	
		return true;
	}
	else if ( shapeID == LINE)
	{
		
		if (( shapeAngle == RIGHT) || (shapeAngle == LEFT) )
		{
			
			//tempPoint[0].x =  shapePoints[1].x;
			tempPoint[0].y -= 1;// shapePoints[0].y-1; // the same
			
			//tempPoint[1].x = shapePoints[1].x ;
			//tempPoint[1].y = shapePoints[1].y ;
			
			tempPoint[2].x =  tempPoint[1].x;
			tempPoint[2].y = tempPoint[1].y+1;
	
			tempPoint[3].x = tempPoint[1].x;
			tempPoint[3].y = tempPoint[1].y+2;
			

		
		}
		else 
		{
			
			tempPoint[0].x -= 1; //  shapePoints[0].x -1;
			//tempPoint[0].y =  shapePoints[0].y;

			//tempPoint[1].x = shapePoints[1].x;
			tempPoint[1].y -= 1; // shapePoints[1].y - 1; 

			tempPoint[2].x += 1; // shapePoints[2].x + 1; 
			tempPoint[2].y -= 2; //shapePoints[2].y - 2;
	
			tempPoint[3].x += 2; // shapePoints[3].x + 2 ;
			tempPoint[3].y -= 3; // shapePoints[3].y - 3;
			
		}
	}


	else if( shapeID == TRIANGLE)
	{

		if ( shapeAngle == RIGHT)
		{
			tempPoint[0].x  -= 1;
			tempPoint[0].y  += 1;
		}

		else if ( shapeAngle == BOTTOM )
		{

			tempPoint[3].x  -= 1;
			tempPoint[3].y  -= 1;

		}

		else if ( shapeAngle == LEFT )
		{
		
			tempPoint[0].x  += 1;
			tempPoint[0].y -= 1;

			tempPoint[1].x -= 1;
			tempPoint[1].y -= 1;


			tempPoint[3].x += 1;
			tempPoint[3].y += 1;

		}

		else if ( shapeAngle == UP)
		{
			tempPoint[1].x += 1;
			tempPoint[1].y += 1;
		}
	}
	else if( shapeID == Z)
	{
		if ( shapeAngle == RIGHT)
		{
			 
			
			tempPoint[0].x += 2;
			tempPoint[0].y -= 1;

			//tempPoint[1].x = 0;
			//tempPoint[1].y += 1;

			//tempPoint[2].x -= 1;
			//tempPoint[2].y = 0;

			//tempPoint[3].x -= 2;
			tempPoint[3].y -= 1;


			 
			 
			 
		}
		else if ( shapeAngle == BOTTOM)
		{
			 
				
			tempPoint[0].x -= 2;
			tempPoint[0].y += 1;

			//tempPoint[1].x -= 1;
			//tempPoint[1].y += 1;

			//tempPoint[2].x = 0;
			//tempPoint[2].y = 0;

			//tempPoint[3].x -= 1;
			tempPoint[3].y += 1;


			 
			 
			 
		}
		else if ( shapeAngle == LEFT)
		{
			 
				
			tempPoint[0].x += 2;
			tempPoint[0].y -= 1;

			//tempPoint[1].x = 0;
			//tempPoint[1].y += 1;

			//tempPoint[2].x -= 1;
			//tempPoint[2].y = 0;

			//tempPoint[3].x -= 2;
			tempPoint[3].y -= 1;



			 
			 
			 
		}
		else if ( shapeAngle == UP)
		{
			 
			
			tempPoint[0].x -= 2;
			tempPoint[0].y += 1;

			//tempPoint[1].x -= 1;
			//tempPoint[1].y += 1;

			//tempPoint[2].x = 0;
			//tempPoint[2].y = 0;

			//tempPoint[3].x -= 1;
			tempPoint[3].y += 1;


			 
			 
			 
		}
	}
	else if( shapeID == REVZ)
	{
		if ( shapeAngle == RIGHT)
		{
			 
			
			tempPoint[0].x -= 2;
			tempPoint[0].y -= 1;

			//tempPoint[1].x = 0;
			//tempPoint[1].y += 1;

			//tempPoint[2].x += 1;
			//tempPoint[2].y = 0;

			//tempPoint[3].x += 1;
			tempPoint[3].y -= 1;


			 
			 
			 
		}
		else if ( shapeAngle == BOTTOM)
		{
			 
				
			tempPoint[0].x += 2;
			tempPoint[0].y += 1;

			//tempPoint[1].x -= 1;
			//tempPoint[1].y += 1;

			//tempPoint[2].x -= 1;
			//tempPoint[2].y = 0;

			//tempPoint[3].x -= 1;
			tempPoint[3].y += 1;


			 
			 
			 
		}
		else if ( shapeAngle == LEFT)
		{
			 
			tempPoint[0].x -= 2;
			tempPoint[0].y -= 1;

			//tempPoint[1].x = 0;
			//tempPoint[1].y += 1;

			//tempPoint[2].x += 1;
			//tempPoint[2].y = 0;

			//tempPoint[3].x += 1;
			tempPoint[3].y -= 1;


			 
			 
			 
		}
		else if ( shapeAngle == UP)
		{
			 
			
				
			tempPoint[0].x += 2;
			tempPoint[0].y += 1;

			//tempPoint[1].x -= 1;
			//tempPoint[1].y += 1;

			//tempPoint[2].x -= 1;
			//tempPoint[2].y = 0;

			//tempPoint[3].x -= 1;
			tempPoint[3].y += 1;

			 
			 
			 
		}
		
	}
	else if( shapeID == L)
	{
		if ( shapeAngle == RIGHT)
		{
			 
				
			//tempPoint[0].x -= 1;
			tempPoint[0].y += 2;

			tempPoint[1].x -= 1;
			tempPoint[1].y += 1;

			//tempPoint[2].x = 0;
			//tempPoint[2].y = 0;

			tempPoint[3].x += 1;
			tempPoint[3].y -= 1;


			 
			 
			 
		}
		else if ( shapeAngle == BOTTOM)
		{
			 
			
			tempPoint[0].x -= 1;
			//tempPoint[0].y -= 0;

			//tempPoint[1].x -= 0;
			tempPoint[1].y -= 1;

			tempPoint[2].x += 1;
			//tempPoint[2].y += 1;

			tempPoint[3].x += 2;
			tempPoint[3].y += 1;


			 
			 
			 
		}
		else if ( shapeAngle == LEFT)
		{
			 
			
			//tempPoint[0].x = 0;
			tempPoint[0].y -= 1;

			tempPoint[1].x += 1;
			//tempPoint[1].y = 0; 

			//tempPoint[2].x -= 1;
			tempPoint[2].y += 1;

			tempPoint[3].x -= 1;
			tempPoint[3].y += 2;


			 
			 
			 
		}
		else if ( shapeAngle == UP)
		{
			 
			
			tempPoint[0].x += 1;
			tempPoint[0].y -= 1;

			//tempPoint[1].x = 0;
			//tempPoint[1].y += 1;

			tempPoint[2].x -= 1;
			tempPoint[2].y -= 1;

			tempPoint[3].x -= 2;
			tempPoint[3].y -= 2;


			 
			 
			 
		}
		
	}
	else if( shapeID == REVL)
	{
			
		if ( shapeAngle == RIGHT)
		{
			 
				
			//tempPoint[0].x = 0;
			//tempPoint[0].y = 0;

			tempPoint[1].x += 1;
			tempPoint[1].y += 1;

			tempPoint[2].x -= 1;
			//tempPoint[2].y = 0;

			tempPoint[3].x -= 2;
			tempPoint[3].y += 1;


			 
			 
			 
		}
		else if ( shapeAngle == BOTTOM)
		{
			 
				
			//tempPoint[0].x = 0;
			//tempPoint[0].y = 0;

			//tempPoint[1].x += 1;
			tempPoint[1].y -= 2;

			tempPoint[2].x -= 1;
			tempPoint[2].y -= 1;

			tempPoint[3].x += 1;
			tempPoint[3].y -= 1;


			 
			 
			 
		}
		else if ( shapeAngle == LEFT)
		{
			 
				
			//tempPoint[0].x -= 1;
			//tempPoint[0].y = 0;

			//tempPoint[1].x += 1;
			//tempPoint[1].y += 1;

			tempPoint[2].x += 2;
			tempPoint[2].y -= 1;

			//tempPoint[3].x += 2;
			tempPoint[3].y -= 3;


			 
			 
			 
		}
		else if ( shapeAngle == UP)
		{
			 
				
			//tempPoint[0].x = 0;
			tempPoint[0].y -= 2;

			tempPoint[1].x -= 1;
			tempPoint[1].y -= 1;
	
			//tempPoint[2].x += 2;
			//tempPoint[2].y -= 1;

			tempPoint[3].x += 1;
			tempPoint[3].y += 1;


			 
			 
			 
		}
		
		
	}


	for (int i = 0 ; i < 4 ; ++i)
		if( map[tempPoint[i].x - gameStartingPoint.x ][tempPoint[i].y - gameStartingPoint.y] == 0 )
				isVaildFourPoint++;

	return (isVaildFourPoint == 4);
}

Point* Shape::generateShape(Point& startPoint, int inputShapeID)
{

	

	if( inputShapeID == SQUARE)
	{
		shapePoints[0].x = startPoint.x;
		shapePoints[0].y = startPoint.y;

		shapePoints[1].x = startPoint.x+1;
		shapePoints[1].y = startPoint.y;

		shapePoints[2].x = startPoint.x;
		shapePoints[2].y = startPoint.y+1;

		shapePoints[3].x = startPoint.x+1;
		shapePoints[3].y = startPoint.y+1;
		shapeAngle = 3 ;

	}

	else if ( inputShapeID == LINE)
	{
		shapePoints[0].x = startPoint.x;
		shapePoints[0].y = startPoint.y;

		shapePoints[1].x = startPoint.x;
		shapePoints[1].y = startPoint.y+1;

		shapePoints[2].x = startPoint.x;
		shapePoints[2].y = startPoint.y+2;

		shapePoints[3].x = startPoint.x;
		shapePoints[3].y = startPoint.y+3;
		shapeAngle = 3;
	}

	else if( inputShapeID == TRIANGLE)
	{
		shapePoints[0].x = startPoint.x;
		shapePoints[0].y = startPoint.y;

		shapePoints[1].x = startPoint.x-1;
		shapePoints[1].y = startPoint.y+1;

		shapePoints[2].x = startPoint.x;
		shapePoints[2].y = startPoint.y+1;

		shapePoints[3].x = startPoint.x+1;
		shapePoints[3].y = startPoint.y+1;
		shapeAngle = 3;
	}
	else if (inputShapeID == Z )
	{
		shapePoints[0].x = startPoint.x;
		shapePoints[0].y = startPoint.y;

		shapePoints[1].x = startPoint.x+1;
		shapePoints[1].y = startPoint.y;

		shapePoints[2].x = startPoint.x+1;
		shapePoints[2].y = startPoint.y+1;

		shapePoints[3].x = startPoint.x+2;
		shapePoints[3].y = startPoint.y+1;

		shapeAngle = 0;
	}
	else if (inputShapeID == REVZ )
	{
		shapePoints[0].x = startPoint.x;
		shapePoints[0].y = startPoint.y;

		shapePoints[1].x = startPoint.x-1;
		shapePoints[1].y = startPoint.y;

		shapePoints[2].x = startPoint.x-1;
		shapePoints[2].y = startPoint.y+1;

		shapePoints[3].x = startPoint.x-2;
		shapePoints[3].y = startPoint.y+1;

		shapeAngle = 0;
	}
	else if (inputShapeID == L )
	{
		shapePoints[0].x = startPoint.x;
		shapePoints[0].y = startPoint.y;

		shapePoints[1].x = startPoint.x;
		shapePoints[1].y = startPoint.y+1;

		shapePoints[2].x = startPoint.x-1;
		shapePoints[2].y = startPoint.y+1;

		shapePoints[3].x = startPoint.x-2;
		shapePoints[3].y = startPoint.y+1;
		shapeAngle = 0;
	}
	else if (inputShapeID == REVL )
	{
		shapePoints[0].x = startPoint.x;
		shapePoints[0].y = startPoint.y;

		shapePoints[1].x = startPoint.x;
		shapePoints[1].y = startPoint.y+1;

		shapePoints[2].x = startPoint.x+1;
		shapePoints[2].y = startPoint.y+1;

		shapePoints[3].x = startPoint.x+2;
		shapePoints[3].y = startPoint.y+1;

		shapeAngle = 0;
	}


	shapeID = inputShapeID;

	return shapePoints;

}

int Shape::getShapeID()
{
	return shapeID;
}

void Shape::setShapeID(int id)
{
	shapeID = id;
}



void Shape::setMap(char** m)
{
	map = m;
}

void Shape::setGameStartingPoint(Point* p)
{
	gameStartingPoint.x = p->x;
	gameStartingPoint.y =  p->y;
	
}


void Shape::saveToMap()
{
	isLastShapeRemoved = false;
	for (int i =0 ; i < 4 ; ++i)
	{
		lastShape[i].x = shapePoints[i].x - gameStartingPoint.x;
		lastShape[i].y = shapePoints[i].y - gameStartingPoint.y;

		map[lastShape[i].x][lastShape[i].y] = 2; // '#' or any char
	}

	draw(shapePoints);
}

Point* Shape::getLastShape()
{
	return lastShape;
}

void Shape::eraseLastShape()
{
	if ( !isLastShapeRemoved)
	{
		for (int i =0 ; i < 4 ; ++i)
		{
			map[lastShape[i].x][lastShape[i].y] = 0; // ' '

			lastShape[i].x +=  gameStartingPoint.x;
			lastShape[i].y +=  gameStartingPoint.y;
		}

		erase(lastShape);
	}
	isLastShapeRemoved = true;
}