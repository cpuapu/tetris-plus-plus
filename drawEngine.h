

#ifndef __DRW_ENG_H_
#define __DRW_ENG_H_

#include "shape.h"
#include "gameParameters.h"

// for testing
//#define DEBUG_A
#undef DEBUG_A

class DrawEngine
{
	int screenWidth, screenHeight;	

	Point shapePoints[4];

	GameParameters* params;

	void gotoxy(int x,int y);
	void gotoxy(Point* p);
	void isCurserVisible(bool visible);

public:
	DrawEngine(GameParameters* p); 
	
	~DrawEngine();


	
	void deleteShape(Point *p);

	void eraseShape(Point* p);
	void drawShape(Point* p);

	void drawMap(char** map);

	void drawScore(int score);
	



};


#endif