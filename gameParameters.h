

#ifndef _GAME_PARA_H_
#define _GAME_PARA_H_


#include "point.h"

enum 
{
	UTD,
	RTL,
	LTR,
	DTU
};

class GameParameters
{
	static int numPlayers;

	int screenWidth, screenHeight;
	int direction; // L , R , U, d
	
	int score;
	

	char leftKey, 
		rightKey,
		downKey,
		upKey,
		rotationKey,
		pauseKey,
		undoKey;

	char shapeStyle; // # default 
	
	void setKeysLTR();
	void setKeysRTL();
	void setKeysUTD();
	void setKeysDTU();
	void setKeys();

	//static int playerNumber;

	Point newShapePosition;
	Point gameStartingPoint;
	Point moveDirection;

	void setMoveDirection();

public:

	GameParameters(int direc = UTD , int width = 12, int height = 20, char charStyle = '#' );
	~GameParameters();

	void increaseScoreBy(int value);
	int getScore();

	

	int getGameDirection();
	Point getMoveDirection(); 
	

	int getScreenWidth();
	int getScreenHeight();
	void setScreenWidth(int w);
	void setScreenHeight(int h);


	char getShapeStyle();

	char getLeftKey();
	char getRightkey();
	char getDownKey();
	char getRotationKey();
	char getPauseKey();
	char getUndoKey();
	char getUpKey();


	void setGameStartingPoint(Point p);
	Point getGameStartingPoint();
	Point getNewShapePostion();
	void setNewShapePosition();

	void setScore(int s);

};

#endif