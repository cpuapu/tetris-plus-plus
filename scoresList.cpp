
#include "scoresList.h"

#include <stdlib.h>
#include <iomanip>
#include <iostream>
using namespace std;

ScoresList::ScoresList() 
{
	for( int i = 0 ; i < 10 ; ++i )
	{
		scores[i].name[0] = '\0';
		scores[i].score = 0;
	}

	size = 0;
	maxScore = 0;
	minScore = 0;
}

void ScoresList::addScore(int score)
{
	//if ( !maxScore && !minScore && !score) // the fist time
	//	maxScore = minScore = score;
	if (isTopTen(score))
	{
		if ( score > maxScore)
		{
			maxScore = score;
		}
		//cout.flush();
		//getPlayerName();
		cout << "Enter Your Name: ";
		//cin.getline(tempPlayer.name,20); // for flushing the input stream 	
		cin.getline(tempPlayer.name,20);
		tempPlayer.score = score;


		insertPlayer(tempPlayer);
		
		tempPlayer.name[0] = '\0';
		tempPlayer.score = 0;

		sort();

	}
	minScore = scores[size-1].score;
}

void ScoresList::printScoreList()
{
	cout << setw(20) << "Player Name" << setw(7) << "Score"<< endl;
	for ( int i = 0 ; i < size ;++i)
	{
		cout << setw(20) << scores[i].name << setw(7) << scores[i].score<< endl;
	}
}

bool ScoresList::isTopTen(int score)
{
	return  ( score > minScore );
}


void ScoresList::getPlayerName()
{

	
}


void ScoresList::insertPlayer(Player& p)
{
	if( size < 10 )	
	{
		
		strncpy(scores[size].name,p.name,20);
		scores[size].score = p.score;
		size++;
		if( size >= 9)
			size = 9;
	}

}


void ScoresList::sort()
{
	qsort(scores,size,sizeof(Player),comparePlayers);
}

int comparePlayers(const void * a, const void * b)
{
	return ( ((Player*)b)->score - ((Player*)a)->score );
}
