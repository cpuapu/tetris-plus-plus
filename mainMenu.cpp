
#include "mainMenu.h"

#include <iostream>
using namespace std;

#include <conio.h>
#include <Windows.h>


MainMenu::MainMenu() :params(0)
{
	
}

MainMenu::~MainMenu()
{
	if ( !params)
		delete params;
	
}


int MainMenu::mainScreen()
{
	int choice;

	do {
		
		system("cls");
		
		cout << "\t\t\tWelcom to Tetris World" <<endl;
		cout << "\t\t\t          ------             " << endl;
		cout << "\t\t\tPlease enter your choice: "<<endl;
		cout << "\t\t\t\t1 - Single-Player"<<endl;
		cout << "\t\t\t\t2 - Multi-Player"<< endl;
		cout << "\t\t\t\t3 - Hight Scores List" << endl;
		cout << "\t\t\t\t4 - Quit"<<endl;
		cout << "\t\t:";
	
		
		cin >> choice;

	
		
		if ( (choice > 4 || choice < 1 ))
		{
			cout << "incorrect choice, Please try again" << endl; 
			cout << "Retry in      ";
			for ( int i = 3 ; i != 0 ; --i)
			{
				cout << "\b\b\b\b\b" << i << " sec";
				Sleep(1000);
			}
		}
		

	
	}while( choice > 4|| choice < 1 );


	return choice;

}

void MainMenu::setParams()
{
	
	
	int w, h,direc;
	char style;
	cout << " --Welcom to Tetris land-- " << endl;
	cout << " ------------------------- " << endl;
	cout << "  enter your choice: " << endl;
	cout << " s - start a new game"<< endl;
	cout << " q - quit" <<endl;
	cout << " : " ;

	char c;
	cin >> c;

	if ( c == 's')
	{


		cout << "Change game settings?[y/n]: ";
		cin >> c;

		if ( ( c== 'y') || ( c== 'Y') )
		{
			
			cout << "change game width(default=12) [y/n]:";
			cin >> c;
			cin.ignore(256,'\n');
			if ( ( c== 'y') || ( c== 'Y') )
			{
				cout << "width : ";
				cin >> w;
				if ( w >= 80 )
					w = 70;
				
			}
			else 
				w = 12;
			
			cout << "change game height(default=20) [y/n]:";
			cin >> c;
			cin.ignore(256,'\n');
			if ( ( c== 'y') || ( c== 'Y') )
			{
				cout << "height : ";
				cin >> h;
				if ( h >= 25)
					h = 20;
			}
			else
				h = 20;
			
			cout << "change game direction(default=0 up-to-down) [y/n]:";
			cin >> c;
			cin.ignore(256,'\n');

			if ( ( c== 'y') || ( c== 'Y') )
			{
				cout << "direction : \n 0 - up-to-down \n 1 - right-to-left \n 2 - left-to-right \n 3 - down-to-up \n : ";
				cin >> direc;
				if(direc > 3  || direc < 0 )
					direc = 0;
			}
			else
				direc = 0;
			
			cout << "change shape char(default= #) [y/n]:";
			cin >> c;
			cin.ignore(256,'\n');

			if ( ( c== 'y') || ( c== 'Y') )
			{
				cout << "char : ";
				cin >> style;
				cin.ignore(256,'\n');
			}
			else
				style = '#';

			if ( ( direc == 1 ) || (direc == 2) )
			{
				c = (char)h;
				h = w;
				w = (int)c;
			}
			params = new GameParameters(direc, w,h,style);

		}
		else
		{
			params = new GameParameters();
		}


		
		system("cls");
	
	}
	else
	{
		system("cls");
		return;
	}
}

GameParameters* MainMenu::getParams()
{
	return params;
}


void MainMenu::startMainScreen()
{

	

}

GameParameters* MainMenu::getSinglePlayerParams()
{
	GameParameters* singleParams;
	char c,style;
	int direc, w,h;

	
		cout << "Change game settings?[y/n]: ";
		cin >> c;

		if ( ( c== 'y') || ( c== 'Y') )
		{
			
			cout << "change game width(default=12) [y/n]:";
			cin >> c;
			cin.ignore(256,'\n');
			if ( ( c== 'y') || ( c== 'Y') )
			{
				cout << "width : ";
				cin >> w;
				if ( w >= 80 )
					w = 70;
				
			}
			else 
				w = 12;
			
			cout << "change game height(default=20) [y/n]:";
			cin >> c;
			cin.ignore(256,'\n');
			if ( ( c== 'y') || ( c== 'Y') )
			{
				cout << "height : ";
				cin >> h;
				if ( h >= 25)
					h = 20;
			}
			else
				h = 20;
			
			cout << "change game direction(default=0 up-to-down) [y/n]:";
			cin >> c;
			cin.ignore(256,'\n');

			if ( ( c== 'y') || ( c== 'Y') )
			{
				cout << "direction : \n 0 - up-to-down \n 1 - right-to-left \n 2 - left-to-right \n 3 - down-to-up \n : ";
				cin >> direc;
				if(direc > 3  || direc < 0 )
					direc = 0;
			}
			else
				direc = 0;
			
			cout << "change shape char(default= #) [y/n]:";
			cin >> c;
			cin.ignore(256,'\n');

			if ( ( c== 'y') || ( c== 'Y') )
			{
				cout << "char : ";
				cin >> style;
				cin.ignore(256,'\n');
			}
			else
				style = '#';

			if ( ( direc == 1 ) || (direc == 2) )
			{
				c = (char)h;
				h = w;
				w = (int)c;
			}
			singleParams = new GameParameters(direc, w,h,style);

		}
		else
		{
			singleParams = new GameParameters();
		}

	


	return singleParams;
}