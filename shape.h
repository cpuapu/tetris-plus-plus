

#ifndef _SHAPE_H_
#define _SHAPE_H_

#include "point.h"
#include "drawEngine.h"


enum ShapeIdList
{
	SQUARE,
	LINE,
	TRIANGLE,
	Z,
	REVZ,
	L,
	REVL // reverse L
};

enum ShapeAngle
{
	RIGHT,
	BOTTOM,
	LEFT,
	UP
};

class DrawEngine;
class Shape
{
	Point shapePoints[4];
	DrawEngine *pde;
	int shapeID;
	int shapeAngle;
	char** map;
	Point gameStartingPoint;
	Point lastShape[4];
	bool isLastShapeRemoved;
	
public:

	Shape(DrawEngine* de, Point* inputShape);
	Shape(DrawEngine* de);
	Shape(DrawEngine* de, Point* inputShape, int inShapeID);	
	~Shape();

	Point* getShape();
	void setShape(Point* p);

	bool move(int x, int y);
	bool move(Point& p);
	bool isValidMove(int x, int y);
	bool isValidMove(Point& p);

	bool rotate();
	bool isValidRotation();

	Point* generateShape(Point& startPoint, int inputShapeID);

	int getShapeID();
	void setShapeID(int id);

	void draw(Point* p);
	void erase(Point* p);
	void eraseLastShape();

	void setMap(char** m);
	void setGameStartingPoint(Point* p);

	void saveToMap();
	Point* getLastShape();
	
};

#endif