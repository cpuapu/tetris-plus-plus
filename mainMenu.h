

#ifndef _MAIN_MENU_H_
#define _MAIN_MENU_H_

#include "gameParameters.h"

class MainMenu
{
	GameParameters* params;

public:

	MainMenu() ;
	~MainMenu();


	int mainScreen();

	void setParams();

	void startMainScreen();

	GameParameters* getParams();

	
	GameParameters* getSinglePlayerParams();
	
};

#endif