


#include "game.h"

#include <iostream>

#include "drawEngine.h"
#include "character.h"
#include "gameParameters.h"
#include "Level.h"

#include <conio.h>
#include <Windows.h>


using namespace std;


Game::Game() : params(0) , gameScores(0)
{

	gameScores = new ScoresList;
	gameSpeed = EASY;
}
Game::~Game()
{
	delete gameScores;
}

void Game::run()
{

	int choice;
	

	START_MENU:

	
	choice = menu.mainScreen();

	if (  choice == 1 ||  choice == 2 )
	{
		int gSpeed;
		system("cls");
		cout << "Please enter the game speed"<<endl;
		cout << "1 - Easy\n2 - Meduim\n3 - Hard"<<endl;
		cout << " : ";
		cin>> gSpeed;

		if ( gSpeed == 1)
			gameSpeed = EASY;
		else if ( gSpeed == 2)
			gameSpeed = MEDUIM;
		else if ( gSpeed = 3 )
			gameSpeed = HARD;
		else 
			gameSpeed = EASY;
		system("cls");
	}

	if ( choice == 1 ) // single player
	{
		
		printGameControls();
		startSinglePlayer();
		system("cls");
		gameScores->printScoreList();
		
		getchar();
		goto START_MENU; // the first lines of this method
	}
	else if( choice == 2) // multi player
	{
		printGameControls();
		startMultiPlayer();
		system("cls");
		gameScores->printScoreList();
		
		getchar();
		goto START_MENU; // the first lines of this method
	}
	else if( choice == 3) // show score list
	{
		system("cls");
		gameScores->printScoreList();
		cout << "press any key for Main Menu";
		getchar();
		getchar();
		goto START_MENU; // the first lines of this method
	
	}
	else if ( choice == 4)
	{
		system("exit");
	}

	/*
	start:
	char key ;
	
	DrawEngine* de = new DrawEngine(params);

	Point startP;
	startP.x = startP.y = 4;
	params->setGameStartingPoint(startP);
	params->setNewShapePosition();

	Level l(params,de);


	l.initLevel();

	 key = ' ';

	while (key != 27 && !l.isEndOfGame() ) // 27 = esc // and not end of game
	{
		while(!getInput(key) && l.isValidMove())
		{

			l.start();
			// shape . move 
			// if not valid move.. save, check lines, generate shape
			Sleep(gameSpeed);
			// sleep

		}
		l.keyPress(key);
		if ( key != 27)
			key = ' ';

		if(  !l.isValidMove() && !l.isEndOfGame())
		{
			l.update();
			
		}
		updateGameSpeed(params);


	}


	system("cls");

	gameScores->addScore(params->getScore());




	system("cls");
	


	cout << "do u want to continue? : y/n :";
	cin>> key;
	if( key == 'y')
	{
		system("cls");
		goto start;
	}
	gameScores->printScoreList();
	


	cout << "\n\n\n\n\nend of the Game"<< endl;

	
	if (!de)
		delete de;
		*/
}



bool Game::getInput(char& c)
{
	if(_kbhit())
	{
		c = _getch();
		return true;
	}

	return false;
}


void Game::updateGameSpeed(GameParameters* prms)
{
	int score = prms->getScore();

	if ( prms->getScore() >= 1000  && prms->getScore() < 2000)
	{
		gameSpeed = MEDUIM;
	}
	else if (prms->getScore() >= 2000)
	{
		gameSpeed = HARD;
	}


}


void Game::startSinglePlayer()
{
	
	char key;
	GameParameters* singleParams = menu.getSinglePlayerParams();
	DrawEngine* de = new DrawEngine(singleParams);


	system("cls");

	Point startP;
	startP.x = startP.y = 4;
	singleParams->setGameStartingPoint(startP);
	singleParams->setNewShapePosition();

	Level l(singleParams,de);


	l.initLevel();

	 key = ' ';

	while (key != 27 && !l.isEndOfGame() ) // 27 = esc // and not end of game
	{
		while(!getInput(key) && l.isValidMove())
		{

			l.start();
			// shape . move 
			// if not valid move.. save, check lines, generate shape
			Sleep(gameSpeed);
			// sleep

		}
		l.keyPress(key);
		if ( key != 27)
			key = ' ';

		if(  !l.isValidMove() && !l.isEndOfGame())
		{
			l.update();
			
		}
		updateGameSpeed(singleParams);
	}

	system("cls");

	gameScores->addScore(singleParams->getScore());


	if (!de)
		delete de;
	if (!singleParams)
		delete singleParams;
}

void Game::startMultiPlayer()
{
	
	char key;
	cout << "Plyaer 1 " <<endl;
	GameParameters* player1 = menu.getSinglePlayerParams();
	cout << "\n\nPlyaer 2 " <<endl;
	GameParameters* player2 = menu.getSinglePlayerParams();

	if ( player1->getScreenHeight() > 20 || player1->getScreenHeight() < 5)
	{
		player1->setScreenHeight(20);
	}
	if ( player2->getScreenHeight() > 20 || player2->getScreenHeight() < 5 )
	{
		player2->setScreenHeight(20);
	}
	if ( player1->getScreenWidth() > 30 ||  player1->getScreenWidth() < 5 )
	{
		player1->setScreenWidth(30);
	}
	if ( player2->getScreenWidth() > 30 ||  player2->getScreenWidth() < 5)
	{
		player2->setScreenWidth(30);
	}
	system("cls");

	DrawEngine* dePlayer1 = new DrawEngine(player1);
	DrawEngine* dePlayer2 = new DrawEngine(player2);


	Point startP;
	startP.x = startP.y = 4;
	player1->setGameStartingPoint(startP);
	player1->setNewShapePosition();

	startP.x = 45;
	player2->setGameStartingPoint(startP);
	player2->setNewShapePosition();


	Level level1(player1,dePlayer1);
	Level level2(player2,dePlayer2);

	level1.initLevel();
	level2.initLevel();

	key = ' ';

	while (key != 27 && !level1.isEndOfGame()  && !level2.isEndOfGame() ) // 27 = esc // and not end of game
	{
		while(!getInput(key) && level1.isValidMove() && level2.isValidMove())
		{

			level1.start();
			level2.start();
			// shape . move 
			// if not valid move.. save, check lines, generate shape
			Sleep(gameSpeed);
			// sleep

		}
		level1.keyPress(key);
		level2.keyPress(key);
		if ( key != 27)
			key = ' ';


		if(  !level1.isValidMove() && !level1.isEndOfGame()) 
		{
			level1.update();
		}
		if(  !level2.isValidMove() && !level2.isEndOfGame()) 
		{
			level2.update();
		}
		updateGameSpeed(player1);
		updateGameSpeed(player2);
	}

	system("cls");
	
	// will put the higher score from two players if equals will not enter the list!
	//  this makes the game more competitive
	if( player1->getScore() > player2->getScore())
		gameScores->addScore(player1->getScore());

	else if( player2->getScore() > player1->getScore())
		gameScores->addScore(player2->getScore());

	
	delete dePlayer1;
	delete dePlayer2;
	delete player1;
	delete player2;
}



void Game::printGameControls()
{
	system("cls");
	

	cout << "Game Controls"<<endl;
	cout << "Player 1\t\t\tPlayer 2"<<endl;
	cout << "  e r t \t\t\t  i o p " <<endl;
	cout << " s d    \t\t\t j k    " <<endl;
	cout << " zx     \t\t\t nm     " <<endl;
	cout << "        \t\t\t        " <<endl;
	cout << "t p - pause\nz n undo\nr o rotate"<<endl;



	cout << "Game starts in      ";
	for(int i = 3 ; i; --i )
	{
		cout << "\b\b\b\b\b" << i << " sec";
		Sleep(1000);
	}

	system("cls");
}