
#include "gameParameters.h"



GameParameters::GameParameters(int direc , int width, int height , char charStyle  )
{

	if (width >= 80 )
		screenWidth = 12;
	else 
		screenWidth = width;
	

	if (height >= 25 )
		screenHeight = 20;
	else
		screenHeight = height;
	
	direction = direc;
	shapeStyle = charStyle;
	

	setMoveDirection();

	numPlayers++;

	setKeys();

	score = 0;

}
GameParameters::~GameParameters()
{
	numPlayers--;
}

int GameParameters::numPlayers = 0;

void GameParameters::setKeysLTR()
{
	if ( numPlayers == 1)
	{
		leftKey = 0;
		rightKey = 'd';
		downKey = 'x';
		upKey = 'e';
		rotationKey = 'r';
		pauseKey = 't';
		undoKey = 'z';
	}
	else if ( numPlayers == 2)
	{
		leftKey = 0;
		rightKey = 'k';
		downKey = 'm';
		upKey = 'i';
		rotationKey = 'o';
		pauseKey = 'p';
		undoKey = 'n';
	}

	/*
	leftKey = 0;
	rightKey = 'd';
	downKey = 'x';
	rotationKey = 'r';
	pauseKey = 'p';
	undoKey = 'c';
	upKey = 'e';
	*/
}

void GameParameters::setKeysRTL()
{
	if ( numPlayers == 1)
	{
		leftKey = 's';
		rightKey = 0;
		downKey = 'x';
		upKey = 'e';
		rotationKey = 'r';
		pauseKey = 't';
		undoKey = 'z';
	}
	else if ( numPlayers == 2)
	{
		leftKey = 'j';
		rightKey = 0;
		downKey = 'm';
		upKey = 'i';
		rotationKey = 'o';
		pauseKey = 'p';
		undoKey = 'n';
	}

	/*
	leftKey = 's';
	rightKey = 0;
	downKey = 'x';
	upKey = 'e';
	rotationKey = 'd';
	pauseKey = 'p';
	undoKey = 'w';
	*/
}

void GameParameters::setKeysUTD()
{
	if ( numPlayers == 1)
	{
		leftKey = 's';
		rightKey = 'd';
		downKey = 'x';
		rotationKey = 'r';
		pauseKey = 't';
		undoKey = 'z';
		upKey = 0;
	}
	else if ( numPlayers == 2)
	{
		leftKey = 'j';
		rightKey = 'k';
		downKey = 'm';
		rotationKey = 'o';
		pauseKey = 'p';
		undoKey = 'n';
		upKey = 0;
	}
	/*
	leftKey = 's';
	rightKey = 'd';
	downKey = 'x';
	rotationKey = 'e';
	pauseKey = 'p';
	undoKey = 'z';
	upKey = 0;
	*/
}

void GameParameters::setKeysDTU()
{
	if ( numPlayers == 1)
	{
		leftKey = 's';
		rightKey = 'd';
		downKey = 0;
		rotationKey = 'r';
		pauseKey = 't';
		undoKey = 'z';
		upKey = 'e';
	}
	else if ( numPlayers == 2)
	{
		leftKey = 'j';
		rightKey = 'k';
		downKey = 0;
		rotationKey = 'o';
		pauseKey = 'p';
		undoKey = 'n';
		upKey = 'i';
	}
	/*
	leftKey = 's';
	rightKey = 'd';
	downKey = 0;
	rotationKey = 'r';
	pauseKey = 'p';
	undoKey = 'w';
	upKey = 'e';
	*/
}

void GameParameters::setKeys()
{
	if ( direction == LTR)
	{
		setKeysLTR();
	}
	else if ( direction == RTL)
	{
		setKeysRTL();
	}
	else if ( direction == UTD)
	{
		setKeysUTD();
	}
	else if ( direction == DTU)
	{
		setKeysDTU();
	}

	
}

void GameParameters::setMoveDirection()
{
	if ( direction == UTD)
	{
		moveDirection.x = 0;
		moveDirection.y = 1;
	}
	else if ( direction == RTL)
	{
		moveDirection.x = -1;
		moveDirection.y = 0;
	}
	else if( direction == LTR)
	{
		moveDirection.x = 1;
		moveDirection.y = 0;
	}
	else if (direction== DTU)
	{
		moveDirection.x = 0;
		moveDirection.y = -1;
	}
}




int GameParameters::getGameDirection()
{
	return direction;
}



int GameParameters::getScreenWidth()
{
	return screenWidth;
}
int GameParameters::getScreenHeight()
{
	return screenHeight;
}

char GameParameters::getShapeStyle()
{
	return shapeStyle;
}


char GameParameters::getLeftKey()
{
	return leftKey;
}

char GameParameters::getRightkey()
{
	return rightKey;
}

char GameParameters::getDownKey()
{
	return downKey;
}

char GameParameters::getRotationKey()
{
	return rotationKey;
}

char GameParameters::getPauseKey()
{
	return pauseKey;
}

char GameParameters::getUndoKey()
{
	return undoKey;
}

char GameParameters::getUpKey()
{
	return upKey;
}

void GameParameters::setGameStartingPoint(Point p)
{
	gameStartingPoint.x = p.x ;
	gameStartingPoint.y = p.y;

}

Point GameParameters::getGameStartingPoint()
{
	return gameStartingPoint;
}

void GameParameters::setNewShapePosition()
{
	
	if ( direction == LTR)
	{
		newShapePosition.x = gameStartingPoint.x + 2;
		newShapePosition.y = gameStartingPoint.y + (screenHeight / 2);
	}
	else if ( direction == RTL)
	{
		newShapePosition.x = gameStartingPoint.x + screenWidth - 2;
		newShapePosition.y = gameStartingPoint.y + (screenHeight / 2);
	}
	else if ( direction == UTD)
	{
		newShapePosition.x = gameStartingPoint.x + (screenWidth / 2 );
		newShapePosition.y = gameStartingPoint.y + 2;
	}
	else if ( direction == DTU)
	{
		newShapePosition.x = gameStartingPoint.x + (screenWidth / 2 );
		newShapePosition.y = gameStartingPoint.y + screenHeight - 6;
	}
}

Point GameParameters::getNewShapePostion()
{
	return newShapePosition;
}

Point GameParameters::getMoveDirection()
{
	return moveDirection;
}


void GameParameters::increaseScoreBy(int value)
{
	score += value;
}


int GameParameters::getScore()
{
	return score;
}


void GameParameters::setScore(int s)
{
	score = s;
}

void GameParameters::setScreenWidth(int w)
{
	screenWidth = w;
}

void GameParameters::setScreenHeight(int h)
{
	screenHeight = h;
}